<!DOCTYPE html>
<html lang="en">
    <head>
        <title>login</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="css/style.css"/>
    </head>
    <body>
        <h1>login</h1>
        
        <form id="id_form" action="login_result.php" method="post">        
            <fieldset>
                <label for="id_user">Input user:</label>
                <input type="text" id="id_user" name="n_user" value=""/>

                <label for="id_password">Input password:</label>
                <input type="password" id="id_password" name="n_password" value=""/>
            </fieldset>
            <fieldset>
                <input type="submit" id="id_login" name="n_login" value="LOGIN"/>
                <input type="reset" id="id_reset" name="n_reset" value="RESET"/>
            </fieldset>
        </form>
    </body>
</html>
