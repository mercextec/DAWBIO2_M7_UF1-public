<?php
    require_once 'php/general-fn.php';
    require_once 'php/tempconv-fn.php';

    $cels=0;
    $fahren=0;

    if (filter_has_var(INPUT_POST, 'n_calc')) {
        $cels=filter_input(INPUT_POST, 'n_cels');
        $fahren=filter_input(INPUT_POST, 'n_fahren');
    }
?>
<!DOCTYPE html>
<html lang="en"> 
    <head>
        <title>Exercise 2</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="css/style.css"/>
        <script src="js/general-fn.js"></script>
    </head>
    <body>
        <h1>Exercise 2. Fahrenheit to Celsius conversion</h1>
        <form id="id_form" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
            <fieldset>
                <label for="id_cels">Input Celsius:</label>
                <input type="text" id="id_cels" name="n_cels" value="<?php echo $cels; ?>"/>

                <label for="id_fahren">Input Fahrenheit:</label>
                <input type="number" id="id_fahren" name="n_fahren" value="<?php echo $fahren; ?>"/>
            </fieldset>
            <fieldset>
                <input type="submit" id="id_calc" name="n_calc" value="calc"/>
                <input type="button" id="id_reset" name="n_reset" value="reset" onClick="form_reset();"/>
            </fieldset>
            <?php
                if (filter_has_var(INPUT_POST, 'n_calc')) {
                    if (valid_input($cels, $fahren)) {
                        $cels_result=fahren_to_cels($fahren);
                        $fahren_result=cels_to_fahren($cels);

                        print_temp($cels, $fahren, $cels_result, $fahren_result);
                    }
                    else {
                        $errors="Celsius and Fahrenheit must be an integer greater than zero";
                        print_errors($errors);
                    }
                }
            ?>
        </form>
    </body>
</html>