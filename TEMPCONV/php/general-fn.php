<?php
    /**
     * prints validation errors
     * @author cagutiprof
     * @version 2017/October
     * @param void
     * @return void
     */
    function print_errors($errors) {  
echo <<<EOT
        <fieldset>
            <legend>Errors</legend>
            
            <table border='1'>
                <caption>Validation errors</caption>
                <tr>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>$errors</td>
                </tr>
            </table>
        </fieldset>
EOT;
    }

