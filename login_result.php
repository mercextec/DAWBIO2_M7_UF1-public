<?php
  session_start();
  
  // user credentials
  $usr="alumne";
  $pass="php"; 
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>login result</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="css/style.css"/>
    </head>
    <body>
        <h1>login result</h1>
        <?php
            if ((filter_has_var(INPUT_POST, 'n_user')) && (filter_has_var(INPUT_POST, 'n_password'))) {
                // clean values
                $user_input=htmlspecialchars(trim(filter_input(INPUT_POST, 'n_user')));  
                $pass_input=htmlspecialchars(trim(filter_input(INPUT_POST, 'n_password')));
                
                // validate values
                if ((strlen($user_input)==0) || (strlen($pass_input)==0)) { // values not provided
                    echo "<p>User and password required</p>";
                    echo "<p>[<a href='login.php'>login</a>]</p>";                      
                }
                else { 
                    if (($user_input===$usr) && ($pass_input===$pass)) { // check values
                        $_SESSION['user_valid']=true;
                        $_SESSION['user']=$user_input;
                        header("Location: logged.php"); // redirect to application and logged page
                        exit;
                    }
                    else { // bad login and redirect to login page
                        echo "<p>Access denied</p>";
                        echo "<p>[<a href='login.php'>login</a>]</p>";
                    }
                }
            }
            else {
                header("Location: login.php");
            }
        ?>
    </body>
</html>
