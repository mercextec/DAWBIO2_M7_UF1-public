<?php
    $arr1 = array(2.4, 2.6, 3.5);
    $arr2 = array(2.4, 2.6, 3.5);

    echo "<h1>Advanced array</h1>";

    echo "<h2>array_map</h2>";
    print_r(array_map('floor', $arr1));
    echo "<br/>";
    print_r(array_map(function ($num1, $num2) { return $num1+$num2; }, $arr1, $arr2));

    echo "<h2>array_walk</h2>";
    array_walk($arr2, function (&$value, $key) { $value=floor($value); }); 
    print_r($arr2);
    echo "<br/>";
    array_walk($arr1, function ($value, $key) { echo "<p>$key - $value</p>"; });

    echo "<h2>array_filter</h2>";
    print_r(array_filter($arr1, function ($num) { return $num>2.5; }));
