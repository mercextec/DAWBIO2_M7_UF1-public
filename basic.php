<?php
    ini_set('display_errors', 'On');
    error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>basic</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="css/style.css"/>
    </head>
    <body>
        <p>[<a href="index.php">back</a>]</p>
        <h1>basic</h1>
        <?php
            $surname="Gutiérrez";
            
            echo "<h2>option 1:</h2><p>Carlos " . $surname . "</p>";

            echo "<h2>option 2:</h2><p>Carlos $surname</p>";

echo <<<EOT
            <h2>option 3:</h2>
            <table border="1">
                <thead>
                    <tr>
                        <th>first name</th>
                        <th>surname</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                       <td>Carlos</td>
                       <td>$surname</td>
                    </tr>
                </tbody>
            </table>
EOT;
            
        ?>
    </body>
</html>