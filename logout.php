<?php
  session_start();

  if (!$_SESSION['user_valid']) {
      header("Location: login.php");
  }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>logout</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="css/style.css"/>
    </head>
    <body>
        <h1>logout</h1>
        <?php
            if (isset($_SESSION['user_valid'])) { // user logged
                // remove all session variables
                session_unset();

                // destroy the session
                session_destroy();

                echo "<p>Logout done</p>";
                echo "<p>[<a href='login.php'>Login</a>]</p>";                
            }
            else { //user not logged yet
                echo "<p>Not logged</p>";
                echo "<p>[<a href='login.php'>Login</a>]</p>";                
            }
        ?>        
    </body>
</html>
